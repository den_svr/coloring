package com.danser.coloring

import android.app.Application
import android.content.Context
import android.media.MediaPlayer
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.room.Room
import com.danser.coloring.data.database.MainDatabase
import com.danser.coloring.domain.interactor.GameInteractor
import com.danser.coloring.domain.repository.*
import com.danser.coloring.utils.AssetStorage
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.FlowCollector

interface MainComponent {
    val levelPreviewsRepo: ILevelPreviewsRepository
    val lockedLevelsRepo: LockedLevelsRepo
    val gameInteractor: GameInteractor
    val musicRepo: IMusicRepository
    val lastLevelRepo: ILastLevelRepository
    val defaultScope: CoroutineScope
    val ioScope: CoroutineScope
}

class MainApplication : Application(), MainComponent {

    private val mediaPlayer: MediaPlayer by lazy {
        MediaPlayer.create(this, R.raw.background_music)
            .apply {
                isLooping = true
            }
    }

    private val assetStorage: AssetStorage by lazy { AssetStorage(this) }
    private val bitmapRepo: BitmapRepo by lazy { BitmapRepoImpl(this) }
    override val gameInteractor: GameInteractor by lazy { provideGameInteractor() }
    override val musicRepo: IMusicRepository by lazy { MusicRepository(this) }
    override val lastLevelRepo: ILastLevelRepository by lazy { LastLevelRepository(this) }
    override val defaultScope: CoroutineScope by lazy { CoroutineScope(Dispatchers.Default) }
    override val ioScope: CoroutineScope by lazy { CoroutineScope(Dispatchers.IO) }
    override val lockedLevelsRepo: LockedLevelsRepo by lazy { LockedLevelsRepoImpl(this) }

    override fun onCreate() {
        super.onCreate()

        setupMusic()
    }

    fun startMusic() {
        if (musicRepo.observeMusicEnabled.value) {
            mediaPlayer.start()
        }
    }

    fun pauseMusic() {
        if (mediaPlayer.isPlaying) {
            mediaPlayer.pause()
        }
    }

    @OptIn(InternalCoroutinesApi::class)
    private fun setupMusic() {
        ProcessLifecycleOwner.get().lifecycle.addObserver(object : LifecycleObserver {
            @OnLifecycleEvent(Lifecycle.Event.ON_START)
            fun onEnterForeground() {
                startMusic()
            }

            @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
            fun onEnterBackground() {
                pauseMusic()
            }
        })
        ioScope.launch {
            musicRepo.observeMusicEnabled.collect(object : FlowCollector<Boolean> {
                override suspend fun emit(isMusicEnabled: Boolean) {
                    if (isMusicEnabled) {
                        startMusic()
                    } else {
                        pauseMusic()
                    }
                }
            })
        }
    }

    private fun provideGameInteractor(): GameInteractor {
        val database = Room.databaseBuilder(this, MainDatabase::class.java, "main_database.db")
            .allowMainThreadQueries()
            .build()
        val gameRepository = AssetsGameRepository(
            assetStorage = assetStorage
        )
        val savedGameRepository = SavedGameRepository(
            database = database
        )
        return GameInteractor(
            gameRepository = gameRepository,
            savedGameRepository = savedGameRepository,
            ioScope = ioScope
        )
    }

    override val levelPreviewsRepo: ILevelPreviewsRepository by lazy {
        LevelPreviewsRepository(
            assetStorage = assetStorage,
            bitmapRepo = bitmapRepo,
            lockedLevelsRepo = lockedLevelsRepo,
            ioScope = ioScope
        )
    }
}

val Context.mainComponent: MainComponent get() = applicationContext as MainComponent
