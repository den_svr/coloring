package com.danser.coloring.presenter

import android.graphics.Color
import com.danser.coloring.domain.model.GameModel
import com.danser.coloring.domain.model.getColoredSegments
import com.danser.coloring.view.adapter.PaletteColorItem
import com.danser.coloring.view.fragment.GameViewModel
import com.danser.coloring.view.view.ColoringViewModel
import com.danser.coloring.view.view.PaletteViewModel

class GameVmFactory {

    fun getViewModel(model: GameModel): GameViewModel = with(model) {
        val coloring = getColoringViewModel()
        val palette = getPaletteViewModel()
        return GameViewModel(
            coloring = coloring,
            palette = palette,
            isFinished = isFinished,
            finishBitmapName = finishBitmapName
        )
    }

    private fun GameModel.getColoringViewModel(): ColoringViewModel {
        val coloredSegments = coloring.getColoredSegments()
        return ColoringViewModel(
            image = coloring.image,
            mask = coloring.mask,
            segments = coloring.segments,
            coloredSegments = coloredSegments
        )
    }

    private fun GameModel.getPaletteViewModel(): PaletteViewModel {
        val colors = coloring.getColoredSegments(colored = false)
            .groupBy { it.colorHex }
            .map { it.value.first() }
            .map { segment ->
                val labelColor = getLabelColor(segment.colorHex)
                PaletteColorItem(
                    color = segment.colorHex,
                    label = segment.colorLabel,
                    labelColor = labelColor,
                    isSelected = segment.colorHex == selectedColor
                )
            }
        return PaletteViewModel(
            colors = colors
        )
    }

    private fun getLabelColor(colorHex: Int): Int {
        val r = colorHex shr 16 and 0xFF
        val g = colorHex shr 8 and 0xFF
        val b = colorHex shr 0 and 0xFF
        val gray =0.2126 * r + 0.7152 * g + 0.0722 * b
        return when {
            gray < 100 -> Color.WHITE
            else -> Color.BLACK
        }
    }
}
