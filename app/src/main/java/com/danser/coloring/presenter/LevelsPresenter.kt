package com.danser.coloring.presenter

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.danser.coloring.domain.model.*
import com.danser.coloring.domain.repository.ILevelPreviewsRepository
import com.danser.coloring.domain.repository.LockedLevelsRepo
import com.danser.coloring.utils.toDimenDp
import com.danser.coloring.view.adapter.LevelItem
import com.danser.coloring.view.fragment.LevelsArgs
import com.danser.coloring.view.fragment.LevelsViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch

class LevelsPresenter(
    private val args: LevelsArgs,
    private val levelPreviewsRepo: ILevelPreviewsRepository,
    private val lockedLevelsRepo: LockedLevelsRepo,
    private val defaultScope: CoroutineScope
) : ViewModel() {

    val levelsLiveData: MutableLiveData<LevelsViewModel> = MutableLiveData()
    val effectsLiveData = MutableLiveData<LevelsEffect>()
    private var levels = Levels()
    private var updateNeeded = true
    var toolbarHeightPx: Int = 0

    init {
        update()
    }

    fun showNextLevel(currentLevelId: String) {
        val level = levels.nextLevel(currentLevelId) ?: return
        showLevel(level = level)
    }

    fun unlockNextLevel(currentLevelId: String) {
        val nextLevelId = levels.nextLevel(currentLevelId)?.id ?: return
        levels = levels.unlockLevel(nextLevelId)
        lockedLevelsRepo.unlock(nextLevelId)
    }

    fun scheduleUpdate() {
        updateNeeded = true
    }

    fun resetLevel(currentLevelId: String) {
        levels.nextLevel(currentLevelId)
        val level = levels.level(currentLevelId) ?: return
        showLevel(level = level)
    }

    fun showLevel(level: Level) {
        if (!level.locked) {
            val args = GameArgs(levelId = level.id)
            effectsLiveData.postValue(LevelsEffect.ShowLevel(args))
        } else {
            effectsLiveData.postValue(LevelsEffect.ShowLockAnimation(level.id))
        }
    }

    fun update() = defaultScope.launch {
        if (!updateNeeded) return@launch
        updateNeeded = false
        levels = levelPreviewsRepo.getLevels()
        levels.levels.getOrNull(0)?.let { lockedLevelsRepo.unlock(it.id) }
        val items = levels.levels.mapIndexed { idx, level ->
            val topMargin = if (idx % 2 == 0) 32.toDimenDp() else 0.toDimenDp()
            val bottomMargin = if (idx % 2 == 0) 0.toDimenDp() else 32.toDimenDp()
            val leftMargin = if (idx % 2 == 0) 16.toDimenDp() else 8.toDimenDp()
            val rightMargin = if (idx % 2 == 0) 8.toDimenDp() else 16.toDimenDp()
            LevelItem(
                name = "",
                level = level,
                topMargin = topMargin,
                bottomMargin = bottomMargin,
                leftMargin = leftMargin,
                rightMargin = rightMargin
            )
        }
        val levelsViewModel = LevelsViewModel(
            items = items
        )
        levelsLiveData.postValue(levelsViewModel)
    }

}

sealed class LevelsEffect {
    var executed: Boolean = false

    class ShowLevel(val args: GameArgs) : LevelsEffect()
    class ShowLockAnimation(val levelId: String) : LevelsEffect()
}
