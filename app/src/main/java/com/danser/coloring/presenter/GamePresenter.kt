package com.danser.coloring.presenter

import android.graphics.Bitmap
import android.graphics.PointF
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.danser.coloring.domain.interactor.GameInteractor
import com.danser.coloring.domain.model.*
import com.danser.coloring.domain.repository.ILastLevelRepository
import com.danser.coloring.domain.repository.ILevelPreviewsRepository
import com.danser.coloring.view.fragment.GameViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import java.io.Serializable

class GamePresenter(
    private val args: GameArgs,
    private val gameInteractor: GameInteractor,
    private val levelPreviewsRepo: ILevelPreviewsRepository,
    lastLevelRepository: ILastLevelRepository,
    private val defaultScope: CoroutineScope
) : ViewModel() {

    private val vmFactory: GameVmFactory = GameVmFactory()

    private var model: GameModel? = null
    private var gameLiveData = MutableLiveData<GameViewModel>()
    private var effectLiveData = MutableLiveData<GameEffect>()

    init {
        defaultScope.launch {
            model = loadGameModel()
            updateModel()
            lastLevelRepository.saveLastLevelId(args.levelId)
        }
    }

    fun colorSelected(color: Int) {
        updateModel {
            copy(
                selectedColor = color
            )
        }
    }

    fun segmentTouched(segment: Segment, touchScreenPoint: PointF): Boolean {
        val model = model ?: return false
        if (model.selectedColor != segment.colorHex) return false
        if (FAST_COLORING_MODE) {
            colorAllSegments(segment.colorHex, touchScreenPoint)
            return true
        }
        val coloring = model.coloring.colorSegment(segment.maskColorHex)
        val selectedColor = coloring.getNextSelectedColor(model.selectedColor)
        updateGame(coloring, selectedColor, touchScreenPoint)
        return true
    }

    fun saveLevelPreview(bitmap: Bitmap) = defaultScope.launch {
        val levelId = model?.coloring?.id ?: return@launch
        levelPreviewsRepo.saveLevelPreview(bitmap, levelId)
        effectLiveData.postValue(GameEffect.UpdateLevelPreviews())
    }

    /**
     * for debug only
     */
    private fun colorAllSegments(colorHex: Int, touchScreenPoint: PointF) {
        val model = model ?: return
        val maskColors = model.coloring.segments.filter { it.colorHex == colorHex }
        var coloring = model.coloring
        maskColors.forEach { segment ->
            coloring = coloring.colorSegment(segment.maskColorHex)
        }
        val selectedColor = coloring.getNextSelectedColor(model.selectedColor)
        updateGame(coloring, selectedColor, touchScreenPoint)
    }

    private fun updateGame(coloring: Coloring, selectedColor: Int, touchScreenPoint: PointF) =
        defaultScope.launch {
            gameInteractor.saveGame(coloring)
            val isFinished = coloring.isColored()
            val konfettiColors = coloring.segments.map { it.colorHex }.toSet().toList()
            effectLiveData.postValue(
                GameEffect.ShowSegmentTouchedAnimation(
                    konfettiColors = konfettiColors,
                    touchScreenPoint = touchScreenPoint
                )
            )
            if (isFinished) {
                effectLiveData.postValue(
                    GameEffect.CompositeEffect(
                        listOf(
                            GameEffect.ShowFinishAnimation(
                                konfettiColors = konfettiColors
                            ),
                            GameEffect.UnlockNextLevel(
                                args.copy(levelId = args.levelId)
                            )
                        )
                    )
                )
            }
            updateModel {
                copy(
                    coloring = coloring,
                    selectedColor = selectedColor,
                    isFinished = isFinished
                )
            }
        }

    fun onNextLevelClicked() {
        effectLiveData.postValue(
            GameEffect.ShowInterstitialAd(
                GameEffect.ShowNextLevel(args.copy(levelId = args.levelId))
            )
        )
    }

    fun onRetryClicked() = defaultScope.launch {
        val coloring = model?.coloring ?: return@launch
        gameInteractor.resetGame(coloring)
        levelPreviewsRepo.resetLevelPreview(coloring.id)
        effectLiveData.postValue(GameEffect.ResetLevel(args.copy(levelId = args.levelId)))
    }

    fun onGoToLevelsClicked() {
        effectLiveData.postValue(GameEffect.GoBack())
    }

    fun observeGameModel(): MutableLiveData<GameViewModel> = gameLiveData
    fun observeEffects(): MutableLiveData<GameEffect> = effectLiveData

    private fun updateModel(mapper: GameModel.() -> GameModel = { this }) {
        val model = model?.mapper() ?: return
        this.model = model
        val viewModel = vmFactory.getViewModel(model)
        gameLiveData.postValue(viewModel)
    }

    private suspend fun loadGameModel(): GameModel = gameInteractor
        .getGame(args.levelId)
        .let { coloring ->
            GameModel(
                coloring = coloring,
                selectedColor = coloring.segments.firstOrNull { !it.colored }?.colorHex ?: -1,
                finishBitmapName = coloring.finishBitmapName,
                isFinished = coloring.isColored()
            )
        }
}

sealed class GameEffect {
    var executed: Boolean = false

    class GoBack : GameEffect()
    class ShowInterstitialAd(val pendingEffect: GameEffect) : GameEffect()
    class ShowNextLevel(val args: GameArgs) : GameEffect()
    class UnlockNextLevel(val args: GameArgs) : GameEffect()
    class UpdateLevelPreviews : GameEffect()
    class ResetLevel(val args: GameArgs) : GameEffect()
    class ShowFinishAnimation(val konfettiColors: List<Int>) : GameEffect()
    class ShowSegmentTouchedAnimation(
        val konfettiColors: List<Int>,
        val touchScreenPoint: PointF
    ) : GameEffect()

    class CompositeEffect(val effects: List<GameEffect>) : GameEffect()
}


data class GameArgs(val levelId: String) : Serializable
