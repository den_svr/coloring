package com.danser.coloring.view.adapter

import androidx.core.view.isVisible
import com.danser.coloring.R
import com.danser.coloring.utils.setDebounceOnClickListener
import com.danser.coloring.utils.toColor
import com.example.delegateadapter.delegate.KDelegateAdapter
import com.example.delegateadapter.delegate.diff.IComparableItem
import kotlinx.android.synthetic.main.item_palette_color.*

class PaletteColorAdapter(
    private val onClick: (color: Int) -> Unit
) : KDelegateAdapter<PaletteColorItem>() {

    override fun getLayoutId(): Int = R.layout.item_palette_color

    override fun isForViewType(items: MutableList<*>, position: Int): Boolean =
        items[position] is PaletteColorItem

    override fun onBind(item: PaletteColorItem, viewHolder: KViewHolder) = with(viewHolder) {
        vPaletteColor.setDebounceOnClickListener { onClick(item.color) }
        vSelected.isVisible = item.isSelected
        tvLabel.text = item.label
        tvLabel.setTextColor(item.labelColor)
        vInternalCircle.setBackColor(item.color.toColor())
    }
}

data class PaletteColorItem(
    val color: Int,
    val label: String,
    val labelColor: Int,
    val isSelected: Boolean
) : IComparableItem {
    override fun id() = color
    override fun content() = this
}


