package com.danser.coloring.view.fragment

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.PointF
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.animation.addListener
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.danser.coloring.MainActivity
import com.danser.coloring.R
import com.danser.coloring.mainComponent
import com.danser.coloring.presenter.GameArgs
import com.danser.coloring.presenter.GameEffect
import com.danser.coloring.presenter.GamePresenter
import com.danser.coloring.utils.AssetStorage
import com.danser.coloring.utils.onMeasured
import com.danser.coloring.utils.setDebounceOnClickListener
import com.danser.coloring.view.fragment.MainFragment.Companion.MUSIC_ANIMATION_MS
import com.danser.coloring.view.view.ColoringViewModel
import com.danser.coloring.view.view.PaletteViewModel
import com.danser.coloring.view.view.ViewModelView
import kotlinx.android.synthetic.main.fragment_game.*
import kotlinx.android.synthetic.main.fragment_game_result.*
import kotlinx.android.synthetic.main.fragment_game_result.view.*
import nl.dionsegijn.konfetti.models.Shape
import nl.dionsegijn.konfetti.models.Size


class GameFragment : Fragment(), ViewModelView<GameViewModel> {

    private val assetStorage by lazy { AssetStorage(requireContext()) }

    private val presenter: GamePresenter by viewModels {
        val args = arguments?.getSerializable(KEY_ARGS) as GameArgs
        GamePresenterFactory(
            context = requireContext(),
            args = args
        )
    }

    private var finishAnimationStarted: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        presenter
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_game, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter.observeGameModel().observe(viewLifecycleOwner, Observer { update(it) })
        presenter.observeEffects().observe(viewLifecycleOwner, Observer { onEffect(it) })

        vPalette.colorSelected = { color -> presenter.colorSelected(color) }
        vGameResult.apply {
            vNextLevel.setDebounceOnClickListener { presenter.onNextLevelClicked() }
            vRetry.setDebounceOnClickListener { presenter.onRetryClicked() }
            vLevels.setDebounceOnClickListener { presenter.onGoToLevelsClicked() }
        }
        vBack.setDebounceOnClickListener {
            activity?.onBackPressed()
        }
    }

    override fun update(model: GameViewModel) = with(model) {
        vColoring.update(coloring)
        vPalette.update(palette)

        vColoring.segmentTouched = { segment, point ->
            presenter.segmentTouched(segment, point)
        }

        vColoring.bitmapUpdated = { bitmap ->
            if (!model.isFinished) {
                presenter.saveLevelPreview(bitmap)
            }
        }

        vGameResult.isVisible = isFinished
        if (model.isFinished) {
            val bitmapName = model.finishBitmapName
            val bitmap = assetStorage.getBitmapFromAsset("levels/$bitmapName")!!
            vGameResult.ivImage.setImageBitmap(bitmap)
            presenter.saveLevelPreview(bitmap)
            if (!finishAnimationStarted) {
                animateImageScale()
                vNext.alpha = 0f
                vNextColored.alpha = 1f
            }
        }
    }

    fun onEffect(effect: GameEffect) {
        if (effect.executed) return
        effect.executed = true
        when (effect) {
            is GameEffect.GoBack -> activity?.onBackPressed()
            is GameEffect.ShowInterstitialAd -> {
                (activity as MainActivity).showInterstitialAd(effect.pendingEffect)
            }
            is GameEffect.ShowNextLevel -> {
                activity?.onBackPressed()
                (targetFragment as? IGameFragmentResult)?.showNextLevel(effect.args.levelId)
            }
            is GameEffect.UnlockNextLevel -> {
                (targetFragment as? IGameFragmentResult)?.unlockNextLevel(effect.args.levelId)
            }
            is GameEffect.UpdateLevelPreviews -> {
                (targetFragment as? IGameFragmentResult)?.scheduleUpdate()
            }
            is GameEffect.ResetLevel -> {
                activity?.onBackPressed()
                (targetFragment as? IGameFragmentResult)?.resetLevel(effect.args.levelId)
            }
            is GameEffect.ShowFinishAnimation -> {
                vColoring.resetScale()
                vGameResult.animateAppearance(visible = true)
                startKonfetti(effect.konfettiColors)
            }
            is GameEffect.ShowSegmentTouchedAnimation -> {
                startSmallKonfetti(
                    colors = effect.konfettiColors,
                    center = effect.touchScreenPoint
                )
            }
            is GameEffect.CompositeEffect -> effect.effects.forEach { onEffect(it) }
        }
    }

    private fun View.animateAppearance(visible: Boolean) {
        finishAnimationStarted = visible
        if (visible) {
            isVisible = true
            val vGameResult = vGameResult.vResultBackground
            vGameResult.alpha = 0f
            val animator = ValueAnimator
                .ofFloat(0f, 1f)
                .setDuration(2000L)
            animator.addUpdateListener { animation ->
                val value = animation.animatedValue as Float
                vGameResult.alpha = value
                vNext.alpha = 1 - value
                vNextColored.alpha = value
            }
            animator.startDelay = 2000L
            animator.addListener(onEnd = {
                animateImageScale()
            })
            animator.start()
        } else {
            isVisible = false
        }
    }

    private fun animateImageScale(reverse: Boolean = false) {
        val ivImage = vGameResult?.ivImage ?: return
        val scale = if (reverse) 1f else 1.1f
        ivImage.animate()
            .scaleX(scale)
            .scaleY(scale)
            .setDuration(MUSIC_ANIMATION_MS)
            .withEndAction {
                animateImageScale(!reverse)
            }
    }

    private fun startKonfetti(colors: List<Int>) {
        val vKonfetti = vKonfetti
        vKonfetti.onMeasured {
            vKonfetti.build()
                .addColors(colors)
                .setDirection(0.0, 359.0)
                .setSpeed(1f, 5f)
                .setFadeOutEnabled(true)
                .setTimeToLive(4000L)
                .addShapes(Shape.Square, Shape.Circle)
                .addSizes(Size(12))
                .setPosition(-50f, vKonfetti.width + 50f, -50f, -50f)
                .burst(900)
        }
    }

    private fun startSmallKonfetti(colors: List<Int>, center: PointF) {
        smallKonfettiView.build()
            .addColors(colors)
            .setDirection(0.0, 359.0)
            .setSpeed(2f, 3f)
            .setFadeOutEnabled(true)
            .setTimeToLive(0L)
            .addShapes(Shape.Square, Shape.Circle)
            .addSizes(Size(10))
            .setPosition(center.x, center.x, center.y, center.y)
            .burst(30)
    }

    companion object {
        private const val KEY_ARGS = "args"

        fun create(args: GameArgs): GameFragment = GameFragment()
            .apply { arguments = Bundle().apply { putSerializable(KEY_ARGS, args) } }
    }
}

data class GameViewModel(
    val coloring: ColoringViewModel,
    val palette: PaletteViewModel,
    val isFinished: Boolean = false,
    val showKonfetti: Boolean = false,
    val finishBitmapName: String
)

class GamePresenterFactory(
    private val context: Context,
    private val args: GameArgs
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T = with(context.mainComponent) {
        GamePresenter(
            args = args,
            gameInteractor = gameInteractor,
            levelPreviewsRepo = levelPreviewsRepo,
            lastLevelRepository = lastLevelRepo,
            defaultScope = defaultScope
        ) as T
    }
}
