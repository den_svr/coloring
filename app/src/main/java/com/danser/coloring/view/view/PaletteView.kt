package com.danser.coloring.view.view

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.danser.coloring.R
import com.danser.coloring.view.adapter.PaletteColorAdapter
import com.danser.coloring.view.adapter.PaletteColorItem
import com.example.delegateadapter.delegate.diff.DiffUtilCompositeAdapter
import kotlinx.android.synthetic.main.view_palette.view.*

class PaletteView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr),
    ViewModelView<PaletteViewModel> {

    var colorSelected: (color: Int) -> Unit = {}

    private var adapter: DiffUtilCompositeAdapter = DiffUtilCompositeAdapter.Builder()
        .add(PaletteColorAdapter { colorSelected(it) })
        .build()

    init {
        clipToPadding = false
        clipChildren = false
        inflate(context, R.layout.view_palette, this)
        rvPalette.adapter = adapter
        rvPalette.layoutManager = LinearLayoutManager(context, RecyclerView.HORIZONTAL, false)
        LinearSnapHelper().attachToRecyclerView(rvPalette)
    }

    override fun update(model: PaletteViewModel) {
        adapter.swapData(model.colors)
    }
}

data class PaletteViewModel(
    val colors: List<PaletteColorItem>
)
