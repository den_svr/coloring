package com.danser.coloring.view.adapter

import androidx.core.view.isVisible
import com.danser.coloring.R
import com.danser.coloring.domain.model.Level
import com.danser.coloring.utils.Resources
import com.danser.coloring.utils.setDebounceOnClickListener
import com.danser.coloring.utils.setMargins
import com.example.delegateadapter.delegate.KDelegateAdapter
import com.example.delegateadapter.delegate.diff.IComparableItem
import kotlinx.android.synthetic.main.item_level.*

class LevelAdapter(private val onClicked: (Level) -> Unit) : KDelegateAdapter<LevelItem>() {

    override fun getLayoutId(): Int = R.layout.item_level

    override fun onBind(item: LevelItem, viewHolder: KViewHolder) = with(viewHolder) {
        tvText.text = item.name
        ivImage.setImageBitmap(item.level.preview)
        vLock.isVisible = item.level.locked
        itemView.setDebounceOnClickListener { onClicked(item.level) }
        itemView.setMargins(
            top = item.topMargin.toPixels(itemView.context),
            bottom = item.bottomMargin.toPixels(itemView.context),
            left = item.leftMargin.toPixels(itemView.context),
            right = item.rightMargin.toPixels(itemView.context)
        )
        Unit
    }

    override fun isForViewType(items: MutableList<*>, position: Int): Boolean =
        items[position] is LevelItem
}

data class LevelItem(
    val name: String,
    val level: Level,
    val topMargin: Resources.Dimen,
    val bottomMargin: Resources.Dimen,
    val leftMargin: Resources.Dimen,
    val rightMargin: Resources.Dimen
) : IComparableItem {
    override fun id() = LevelItem::class
    override fun content() = this
}
