package com.danser.coloring.view.fragment

import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.animation.ValueAnimator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.danser.coloring.MainActivity
import com.danser.coloring.R
import com.danser.coloring.domain.repository.ILevelPreviewsRepository
import com.danser.coloring.domain.repository.LockedLevelsRepo
import com.danser.coloring.mainComponent
import com.danser.coloring.presenter.GameArgs
import com.danser.coloring.presenter.LevelsEffect
import com.danser.coloring.presenter.LevelsPresenter
import com.danser.coloring.utils.*
import com.danser.coloring.view.adapter.LevelAdapter
import com.danser.coloring.view.adapter.LevelItem
import com.example.delegateadapter.delegate.CompositeDelegateAdapter
import com.example.delegateadapter.delegate.diff.IComparableItem
import kotlinx.android.synthetic.main.fragment_levels.*
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.android.synthetic.main.item_level.view.*
import kotlinx.coroutines.CoroutineScope
import java.io.Serializable

class LevelsFragment : Fragment(), IGameFragmentResult {

    private val args by lazy { arguments?.getSerializable(KEY_ARGS) as LevelsArgs }

    private val presenter: LevelsPresenter by viewModels {
        with(requireContext().mainComponent) {
            LevelsPresenterFactory(
                args = args,
                levelPreviewsRepo = levelPreviewsRepo,
                lockedLevelsRepo = lockedLevelsRepo,
                defaultScope = defaultScope
            )
        }
    }

    private val adapter: CompositeDelegateAdapter<Any> = CompositeDelegateAdapter.Builder<Any>()
        .add(LevelAdapter { presenter.showLevel(it) })
        .add(DividerAdapter)
        .build()

    private var items: List<Any> = emptyList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val levelId = args.levelIdOnStart
        if (levelId != null) {
            showLevel(GameArgs(levelId))
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_levels, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupRecycler()

        vBack.setDebounceOnClickListener {
            activity?.onBackPressed()
        }

        presenter.levelsLiveData.observe(viewLifecycleOwner, Observer { update(it) })
        presenter.effectsLiveData.observe(viewLifecycleOwner, Observer { onEffect(it) })

        val vToolbar = vToolbar
        presenter.toolbarHeightPx = vToolbar.height
    }

    override fun onStart() {
        super.onStart()
        presenter.update()
    }

    override fun showNextLevel(currentLevelId: String) {
        hideForShortTime()
        presenter.showNextLevel(currentLevelId)
    }

    override fun unlockNextLevel(currentLevelId: String) {
        presenter.unlockNextLevel(currentLevelId)
    }

    override fun scheduleUpdate() {
        presenter.scheduleUpdate()
    }

    override fun resetLevel(currentLevelId: String) {
        hideForShortTime()
        presenter.resetLevel(currentLevelId)
    }

    private fun update(model: LevelsViewModel) {
        vToolbar.onMeasured {
            val divider = DividerItem(
                height = (vToolbar.height + 48.dpToPx()).toDimenPx()
            )
            val bottomDivider = DividerItem(
                height = 32.toDimenDp()
            )
            val items =
                listOf(divider, divider) + model.items + listOf(bottomDivider, bottomDivider)
            this.items = items
            adapter.swapData(items)
        }
    }

    private fun onEffect(effect: LevelsEffect) {
        if (effect.executed) return
        effect.executed = true
        when (effect) {
            is LevelsEffect.ShowLevel -> showLevel(effect.args)
            is LevelsEffect.ShowLockAnimation -> showLockAnimation(effect.levelId)
        }
    }

    private fun showLockAnimation(levelId: String) {
        val position = this.items.indexOfFirst { it is LevelItem && it.level.id == levelId }
        val viewHolder = rvList.findViewHolderForAdapterPosition(position)
        viewHolder?.itemView?.vLock?.animateLock()
    }

    private fun View.animateLock() {
        val scaleX = PropertyValuesHolder.ofFloat(View.SCALE_X, 1f, 1.2f)
        val scaleY = PropertyValuesHolder.ofFloat(View.SCALE_Y, 1f, 1.2f)
        ObjectAnimator.ofPropertyValuesHolder(this, scaleX, scaleY).apply {
            duration = MainFragment.MUSIC_ANIMATION_MS
            repeatCount = 3
            repeatMode = ValueAnimator.REVERSE
            start()
        }
    }

    private fun setupRecycler() {
        rvList.adapter = adapter
        rvList.layoutManager = GridLayoutManager(context, 2)
    }

    private fun showLevel(args: GameArgs) {
        val fragment = GameFragment.create(args)
        fragment.setTargetFragment(this, 1)
        (activity as MainActivity).replaceFragment(fragment)
    }

    private fun hideForShortTime() {
        vLevelsRoot.isVisible = false
        vLevelsRoot.postDelayed({
            vLevelsRoot?.isVisible = true
        }, 100L)
    }

    companion object {
        private const val KEY_ARGS = "args"

        fun create(args: LevelsArgs): LevelsFragment = LevelsFragment()
            .apply { arguments = Bundle().apply { putSerializable(KEY_ARGS, args) } }
    }
}

data class LevelsViewModel(
    val items: List<IComparableItem>
)

data class LevelsArgs(val levelIdOnStart: String?) : Serializable

interface IGameFragmentResult {
    fun showNextLevel(currentLevelId: String)
    fun unlockNextLevel(currentLevelId: String)
    fun scheduleUpdate()
    fun resetLevel(currentLevelId: String)
}

class LevelsPresenterFactory(
    private val args: LevelsArgs,
    private val levelPreviewsRepo: ILevelPreviewsRepository,
    private val lockedLevelsRepo: LockedLevelsRepo,
    private val defaultScope: CoroutineScope
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T =
        LevelsPresenter(
            args = args,
            levelPreviewsRepo = levelPreviewsRepo,
            lockedLevelsRepo = lockedLevelsRepo,
            defaultScope = defaultScope
        ) as T
}


