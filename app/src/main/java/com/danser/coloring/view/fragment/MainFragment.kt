package com.danser.coloring.view.fragment

import android.animation.ObjectAnimator
import android.animation.PropertyValuesHolder
import android.animation.ValueAnimator.INFINITE
import android.animation.ValueAnimator.REVERSE
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.SCALE_X
import android.view.View.SCALE_Y
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import com.danser.coloring.MainActivity
import com.danser.coloring.R
import com.danser.coloring.domain.model.FAST_COLORING_MODE
import com.danser.coloring.domain.repository.ILastLevelRepository
import com.danser.coloring.domain.repository.IMusicRepository
import com.danser.coloring.mainComponent
import com.danser.coloring.utils.setDebounceOnClickListener
import kotlinx.android.synthetic.main.fragment_main.*
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.launch


class MainFragment : Fragment() {

    private val musicRepo: IMusicRepository by lazy { requireContext().mainComponent.musicRepo }
    private val lastLevelRepo: ILastLevelRepository by lazy {
        requireContext().mainComponent.lastLevelRepo
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = inflater.inflate(R.layout.fragment_main, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vPlay.setDebounceOnClickListener {
            val lastLevelId = lastLevelRepo.getLastLevelId()
            val args = LevelsArgs(levelIdOnStart = lastLevelId)
            (activity as MainActivity).replaceFragment(LevelsFragment.create(args))
        }
        vLevels.setDebounceOnClickListener {
            val args = LevelsArgs(levelIdOnStart = null)
            (activity as MainActivity).replaceFragment(LevelsFragment.create(args))
        }
        vMusic.setOnClickListener {
            musicRepo.switchMusic()
        }

        bFastColoring.isVisible = false
        bFastColoring.setDebounceOnClickListener {
            FAST_COLORING_MODE = !FAST_COLORING_MODE
        }
        bFastColoring.isChecked = false

        animatePlayButton()

        observeMusic()
    }

    private fun animatePlayButton() {
        val scaleX = PropertyValuesHolder.ofFloat(SCALE_X, 1f, 1.1f)
        val scaleY = PropertyValuesHolder.ofFloat(SCALE_Y, 1f, 1.1f)
        ObjectAnimator.ofPropertyValuesHolder(vPlay, scaleX, scaleY).apply {
            duration = MUSIC_ANIMATION_MS
            repeatCount = INFINITE
            repeatMode = REVERSE
            start()
        }
    }

    @OptIn(InternalCoroutinesApi::class)
    private fun observeMusic() {
        lifecycleScope.launch {
            musicRepo.observeMusicEnabled.collect(object : FlowCollector<Boolean> {
                override suspend fun emit(isMusicEnabled: Boolean) {
                    val drawable = if (isMusicEnabled) R.drawable.music else R.drawable.no_music
                    vMusic.setImageResource(drawable)
                }
            })
        }
    }

    companion object {
        const val MUSIC_ANIMATION_MS = 725L
    }
}
