package com.danser.coloring.view.view

import android.content.Context
import android.graphics.*
import android.os.Handler
import android.os.HandlerThread
import android.os.Looper
import android.util.AttributeSet
import android.widget.FrameLayout
import android.widget.Toast
import com.danser.coloring.R
import com.danser.coloring.domain.model.Segment
import com.danser.coloring.utils.toColor
import com.danser.coloring.utils.toColorHex
import kotlinx.android.synthetic.main.view_coloring.view.*


class ColoringView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr),
    ViewModelView<ColoringViewModel> {

    /**
     * returns true if need to intercept touch
     */
    var segmentTouched: (segment: Segment, screenPoint: PointF) -> Boolean = { _, _ -> false }
    var bitmapUpdated: (bitmap: Bitmap) -> Unit = {}

    private var model: ColoringViewModel? = null
    private var canvasBitmap: Canvas? = null
    private var bitmap: Bitmap? = null
    private var pixels: IntArray = IntArray(0)
    private var screenTouchPoint = PointF(0f, 0f)

    private val thread = HandlerThread("ColoringView thread")
        .apply { start() }

    init {
        inflate(context, R.layout.view_coloring, this)
        ivImage.setOnViewTapListener { view, x, y ->
            screenTouchPoint = PointF(x + view.x, y + view.y)
        }
        ivImage.setOnPhotoTapListener { _, x, y ->
            onPhotoTap(
                bitmapPoint = PointF(x, y),
                screenPoint = screenTouchPoint
            )
        }
    }

    override fun update(model: ColoringViewModel) {
        if (this.canvasBitmap == null) {
            //init only for first time
            val mutableBitmap = model.image.copy(Bitmap.Config.ARGB_8888, true)
            this.bitmap = mutableBitmap
            this.canvasBitmap = Canvas(mutableBitmap)
            ivImage.setImageBitmap(mutableBitmap)
        }
        if (model.mask != this.model?.mask) {
            val mask = model.mask
            pixels = IntArray(mask.width * mask.height)
            mask.getPixels(pixels, 0, mask.width, 0, 0, mask.width, mask.height)
        }
        if (model != this.model) {
            if (model.coloredSegments != this.model?.coloredSegments) {
                drawSegments(model.coloredSegments)
            }
            this.model = model
        }
    }

    fun resetScale() {
        ivImage.setZoomTransitionDuration(2000)
        ivImage.setScale(1f, true)
    }

    private fun onPhotoTap(bitmapPoint: PointF, screenPoint: PointF): Boolean {
        val model = model ?: return false

        val maskColor = model.mask.getPixelColor(bitmapPoint.x, bitmapPoint.y)
        val maskColorHex = maskColor?.toColorHex()
        if (maskColorHex == null || maskColorHex == Color.WHITE) return false

        val segment = model.segments.firstOrNull { item ->
            item.maskColorHex == maskColorHex
        }

        if (segment != null) {
            return segmentTouched(segment, screenPoint)
        }
        return false
    }

    private fun showColorToast(colorHex: Int) {
        val r = (colorHex shr 16 and 0xFF).toString()
        val g = (colorHex shr 8 and 0xFF).toString()
        val b = (colorHex shr 0 and 0xFF).toString()
        val sr = if (r.length < 2) "0$r" else r
        val sg = if (g.length < 2) "0$g" else g
        val sb = if (b.length < 2) "0$b" else b
        Toast.makeText(context, "0x$sr$sg$sb", Toast.LENGTH_SHORT).show()
    }

    private fun drawSegments(segments: List<Segment>) {
        Handler(thread.looper).post {
            val mask = model?.mask ?: return@post
            val canvas = canvasBitmap ?: return@post

            val paintsMap: Map<Int, Pair<Int, ArrayList<Float>>> = segments.map { segment ->
                segment.maskColorHex.toColor() to (segment.colorHex.toColor() to arrayListOf<Float>())
            }.toMap()

            pixels.forEachIndexed { idx, pixel ->
                val points = paintsMap[pixel]?.second
                points?.add((idx % mask.width).toFloat())
                points?.add(idx / mask.width.toFloat())
            }
            val paint = Paint()
            paintsMap.values.forEach { (colorHex, points) ->
                paint.color = colorHex
                canvas.drawPoints(points.toFloatArray(), paint)
            }
            Handler(Looper.getMainLooper()).post {
                invalidate()
            }
            bitmap?.let { bitmapUpdated(it) }
        }
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        thread.interrupt()
    }
}

data class ColoringViewModel(
    val image: Bitmap,
    val mask: Bitmap,
    val segments: List<Segment>,
    val coloredSegments: List<Segment>
)


fun Bitmap.getPixelColor(x: Float, y: Float): Int? {
    val x = x * width
    val y = y * height
    if (x < 0 || y < 0 || x >= width || y >= height) return null
    return getPixel(x.toInt(), y.toInt())
}
