package com.danser.coloring.view.view

interface ViewModelView<ViewModel> {
    fun update(newState: ViewModel)
}
