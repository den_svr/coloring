package com.danser.coloring.utils.drawme

import androidx.annotation.ColorInt
import com.danser.coloring.utils.drawme.FixedDrawMe

interface FixedDrawMeText : FixedDrawMe {

    fun setTextColor(@ColorInt color: Int)
    fun setTextColorPressed(@ColorInt color: Int)
    fun setTextColorDisabled(@ColorInt color: Int)

}
