package com.danser.coloring.utils

import android.content.Context
import android.content.res.Resources
import android.view.View
import android.view.ViewGroup
import androidx.annotation.ColorRes
import androidx.annotation.DimenRes
import androidx.annotation.Px
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat

fun Float.dpToPx(): Float = this * Resources.getSystem().displayMetrics.density

fun Int.dpToPx(): Int = this * Resources.getSystem().displayMetrics.density.toInt()

fun View.pixels(@DimenRes res: Int): Int = resources.getDimensionPixelSize(res)

fun Context.pixels(@DimenRes res: Int): Int = resources.getDimensionPixelSize(res)

fun View.color(@ColorRes res: Int): Int = ContextCompat.getColor(context, res)

fun Context.color(@ColorRes res: Int): Int = ContextCompat.getColor(this, res)

fun Context.string(@StringRes res: Int): String = getString(res)

fun Context.string(@StringRes res: Int, vararg args: Any): String = getString(res, *args)

fun View.onMeasured(block: () -> Unit): View.OnLayoutChangeListener? {
    return if (measuredWidth > 0) {
        block.invoke()
        null
    } else {
        val layoutChangeListener = object : View.OnLayoutChangeListener {
            override fun onLayoutChange(
                v: View?, left: Int, top: Int, right: Int, bottom: Int,
                oldLeft: Int, oldTop: Int, oldRight: Int, oldBottom: Int
            ) {
                removeOnLayoutChangeListener(this)
                block.invoke()
            }
        }
        addOnLayoutChangeListener(layoutChangeListener)
        layoutChangeListener
    }
}

fun View.setMargins(
    @Px left: Int? = null,
    @Px top: Int? = null,
    @Px right: Int? = null,
    @Px bottom: Int? = null
) =
    (layoutParams as? ViewGroup.MarginLayoutParams)?.apply {
        setMargins(
            left ?: leftMargin,
            top ?: topMargin,
            right ?: rightMargin,
            bottom ?: bottomMargin
        )
    }
