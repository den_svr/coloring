package com.danser.coloring.utils.drawme

import android.content.Context
import android.content.res.Configuration
import android.util.AttributeSet
import android.widget.ImageView

/**
 * @see [FixedDrawMeViewDelegate]
 * @author airfreshener on 18.05.2018.
 */
class FixedDrawMeImageView @JvmOverloads constructor (
        context: Context,
        attrs: AttributeSet? = null,
        defStyleAttr: Int = 0
) : ImageView(context, attrs, defStyleAttr),
        IFixedDrawMeViewDelegate by FixedDrawMeViewDelegate() {

    init { initDrawMe(this, attrs) }

    private var configurationChangedListener: (FixedDrawMeImageView.()-> Unit)? = null

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val size = onMeasureView(widthMeasureSpec, heightMeasureSpec)
        super.onMeasure(size[0], size[1])
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        onLayoutView(changed, left, top, right, bottom)
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        configurationChangedListener?.invoke(this)
    }

    fun setOnConfigurationChangedListener(listener: FixedDrawMeImageView.() -> Unit) {
        configurationChangedListener = listener
    }

}
