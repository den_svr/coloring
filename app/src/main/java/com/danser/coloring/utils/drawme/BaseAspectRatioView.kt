package com.danser.coloring.utils.drawme

import android.util.AttributeSet
import android.view.View
import com.danser.coloring.R

typealias AspectRatio = Float
class BaseAspectRatioView : IAspectRatioView {
    /**
     * Regardless of the ratio mode, the ratio means height-to-width.
     * So, width = height / ratio, height = width * ratio
     * */
    private var ratio = .75f
    private var ratioMode = IAspectRatioView.RatioMode.BY_WIDTH
    private lateinit var view: View

    override fun initAspectRatio(view: View, attrs: AttributeSet?) {
        this.view = view
        val a = view.context.obtainStyledAttributes(attrs, R.styleable.AspectRatioView)
        setRatio(a.getFloat(R.styleable.AspectRatioView_ratioValue, ratio))
        val intMode = a.getInteger(R.styleable.AspectRatioView_ratioMode, ratioMode.id)
        setRatioMode(IAspectRatioView.RatioMode.valueOf(intMode))
        a.recycle()
    }

    override fun setRatio(ratio: AspectRatio) {
        require(ratio >= 0.0) { "ratio should be positive!" }
        this.ratio = ratio
        view.requestLayout()
    }

    override fun setRatioMode(ratioMode: IAspectRatioView.RatioMode) {
        this.ratioMode = ratioMode
        view.requestLayout()
    }

    override fun measureView(widthSpec: Int, heightSpec: Int): IntArray {
        val widthPadding = view.paddingLeft + view.paddingRight
        val heightPadding = view.paddingTop + view.paddingBottom
        return getMeasureSpec(widthSpec, heightSpec, ratioMode, ratio, widthPadding, heightPadding)
    }

    private fun getMeasureSpec(
        widthSpec: Int,
        heightSpec: Int,
        ratioMode: IAspectRatioView.RatioMode,
        aspectRatio: Float,
        widthPadding: Int,
        heightPadding: Int): IntArray {
        if (aspectRatio <= 0) {
            return intArrayOf(widthSpec, heightSpec)
        }
        var newWidthSpec = widthSpec
        var newHeightSpec = heightSpec
        if (ratioMode == IAspectRatioView.RatioMode.BY_WIDTH) {
            val widthSpecSize = View.MeasureSpec.getSize(widthSpec)
            val desiredHeight = ((widthSpecSize - widthPadding) * aspectRatio + heightPadding).toInt()
            val resolvedHeight = View.resolveSize(desiredHeight, 0)
            newHeightSpec = resolvedHeight.toExactly()
        } else if (ratioMode == IAspectRatioView.RatioMode.BY_HEIGHT) {
            val heightSpecSize = View.MeasureSpec.getSize(heightSpec)
            val desiredWidth = ((heightSpecSize - heightPadding) / aspectRatio + widthPadding).toInt()
            val resolvedWidth = View.resolveSize(desiredWidth, 0)
            newWidthSpec = resolvedWidth.toExactly()
        }
        return intArrayOf(newWidthSpec, newHeightSpec)
    }

}
