package com.danser.coloring.utils.listener

import java.io.Serializable

interface ChooseListener<in T> : Serializable, (T?) -> Unit

abstract class ContextedChooseListener<Args : Serializable, T>(private val args: Args) :
    ChooseListener<T> {

    override operator fun invoke(obj: T?) {
        onChosenWithContext(args, obj)
    }

    abstract fun onChosenWithContext(args: Args, obj: T?)
}

inline fun <T> buildChooseListener(crossinline exec: (chosen: T?) -> Unit): ChooseListener<T> = object :
    ChooseListener<T> {
    override operator fun invoke(obj: T?) {
        exec(obj)
    }
}

inline fun <Args : Serializable, T> buildChooseListener(
    args: Args,
    crossinline exec: (args: Args, chosen: T?) -> Unit
): ContextedChooseListener<Args, T> = object : ContextedChooseListener<Args, T>(args) {
    override fun onChosenWithContext(args: Args, obj: T?) {
        exec(args, obj)
    }
}
