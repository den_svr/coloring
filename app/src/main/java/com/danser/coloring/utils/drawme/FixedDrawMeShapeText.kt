package com.danser.coloring.utils.drawme

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.util.AttributeSet
import android.view.View
import androidx.annotation.AttrRes
import androidx.annotation.ColorInt
import com.danser.coloring.utils.drawme.FixedDrawMeText
import com.rafakob.drawme.R
import com.rafakob.drawme.delegate.DrawMeShapeText
import com.rafakob.drawme.util.Coloring

class FixedDrawMeShapeText @JvmOverloads constructor(
        context: Context,
        view: View,
        attrs: AttributeSet? = null,
        @AttrRes defStyleAttr: Int = 0
) : DrawMeShapeText(context, view, attrs, defStyleAttr), FixedDrawMeText {

    private var maskPressedColor = Color.parseColor("#1F000000")

    override fun setTextColor(@ColorInt color: Int) {
        this.textColor = color
    }

    override fun setTextColorPressed(@ColorInt color: Int) {
        this.textColorPressed = color
    }

    override fun setTextColorDisabled(@ColorInt color: Int) {
        this.textColorDisabled = color
    }

    override fun setBackgroundColor(@ColorInt color: Int) {
        this.backColor = color
        updateBackColors()
        updateLayout()
    }

    override fun getBackgroundColor(): Int = this.backColor

    override fun setStrokeColor(@ColorInt color: Int) {
        this.strokeColor = color
    }

    override fun setStrokeWidth(width: Int) {
        this.stroke = width
    }

    override fun setCornersRadius(tl: Int, tr: Int, bl: Int, br: Int){
        this.radiusTopLeft = tl
        this.radiusTopRight = tr
        this.radiusBottomLeft = bl
        this.radiusBottomRight = br
    }

    override fun setRippleEffectEnabled(enabled: Boolean) {
        rippleEffect = enabled
        rippleUseControlHighlight = enabled
        maskColorPressed = if (enabled) this.maskPressedColor else Color.TRANSPARENT
        updateBackColors()
        updateLayout()
    }

    override fun setRippleColor(color: Int) {
        maskColorPressedInverse = color
        maskPressedColor = color
        updateBackColors()
        updateLayout()
    }

    private fun updateBackColors() {
        backColorPressed = defaultPressedColor(backColor)
        backColorDisabled = defaultDisabledColor(backColor)
    }

    @ColorInt
    private fun defaultPressedColor(normalColor: Int): Int = when {
        maskBrightnessThreshold > 0.0f && Coloring.getColorBrightness(normalColor) < maskBrightnessThreshold -> {
            Coloring.mix(maskColorPressedInverse, normalColor)
        }
        Build.VERSION.SDK_INT >= 21 && rippleUseControlHighlight -> Coloring.getThemeColor(
            mView.context,
            R.attr.colorControlHighlight
        )
        else -> Coloring.mix(maskColorPressed, normalColor)
    }

    @ColorInt
    private fun defaultDisabledColor(normalColor: Int): Int = Coloring.mix(maskColorDisabled, normalColor)

}
