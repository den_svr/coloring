package com.danser.coloring.utils

import android.content.Context
import androidx.annotation.*
import java.io.Serializable

object Resources {
    sealed class Color : Serializable {
        abstract fun toColorInt(context: Context): Int

        data class ResId(
            @ColorRes private val colorResId: Int
        ) : Color() {
            override fun toColorInt(context: Context): Int = context.color(colorResId)
        }

        data class Literal(
            @ColorInt private val colorInt: Int
        ) : Color() {
            override fun toColorInt(context: Context): Int = colorInt
        }

        companion object {
            val TRANSPARENT = Literal(android.graphics.Color.TRANSPARENT)
        }
    }

    sealed class Text : Serializable {
        abstract fun toString(context: Context): String

        data class ResId(
            @StringRes private val stringResId: Int,
            private val placeHolders: List<Text> = listOf()
        ) : Text() {
            constructor(
                stringResId: Int,
                vararg placeHolders: Any
            ) : this(
                stringResId,
                placeHolders.map { if (it is Text) it else Literal(it.toString()) })

            @Suppress("SpreadOperator")
            override fun toString(context: Context): String = when (placeHolders.isEmpty()) {
                true -> context.string(stringResId)
                false -> context.string(
                    stringResId,
                    *placeHolders.map { it.toString(context) }.toTypedArray()
                )
            }
        }

        data class Literal(
            private val literal: String
        ) : Text() {
            override fun toString(context: Context): String = literal
        }

        private data class Concatenation(
            private val first: Text,
            private val second: Text
        ) : Text() {
            override fun toString(context: Context): String =
                first.toString(context) + second.toString(context)
        }

        operator fun plus(other: Text): Text = Concatenation(this, other)
    }

    sealed class Dimen : Serializable {
        abstract fun toPixels(context: Context): Int

        data class ResId(
            @DimenRes private val dimenResId: Int
        ) : Dimen() {
            override fun toPixels(context: Context): Int = context.pixels(dimenResId)
        }

        data class Dp(
            private val dp: Int
        ) : Dimen() {
            override fun toPixels(context: Context): Int = dp.dpToPx()
        }

        data class Pixels(
            private val pixels: Int
        ) : Dimen() {
            override fun toPixels(context: Context): Int = pixels
        }
    }

    sealed class DrawableResource : Serializable {
        data class ResId(
            @DrawableRes val resId: Int
        ) : DrawableResource()

        data class Url(
            val url: String
        ) : DrawableResource()
    }
}

fun String.toRes(): Resources.Text = Resources.Text.Literal(this)
fun String?.toResOrNull(): Resources.Text? =
    if (this == null) null else Resources.Text.Literal(this)

fun Int.toTextRes(): Resources.Text = Resources.Text.ResId(this)
fun Int.toDimenRes(): Resources.Dimen = Resources.Dimen.ResId(this)
fun Int.toDimenDp(): Resources.Dimen = Resources.Dimen.Dp(this)
fun Int.toDimenPx(): Resources.Dimen = Resources.Dimen.Pixels(this)
fun Int.toColorRes(): Resources.Color = Resources.Color.ResId(this)
