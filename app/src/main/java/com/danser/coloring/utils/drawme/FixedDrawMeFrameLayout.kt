package com.danser.coloring.utils.drawme

import android.content.Context
import android.util.AttributeSet
import android.widget.FrameLayout

/**
 * @see [FixedDrawMeViewDelegate]
 */
open class FixedDrawMeFrameLayout @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr),
    IFixedDrawMeViewDelegate by FixedDrawMeViewDelegate() {

    init {
        initDrawMe(this, attrs, defStyleAttr)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val size = onMeasureView(widthMeasureSpec, heightMeasureSpec)
        super.onMeasure(size[0], size[1])
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        onLayoutView(changed, left, top, right, bottom)
    }

}
