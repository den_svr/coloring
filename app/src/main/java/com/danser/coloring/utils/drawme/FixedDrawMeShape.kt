package com.danser.coloring.utils.drawme

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.util.AttributeSet
import android.view.View
import androidx.annotation.AttrRes
import androidx.annotation.ColorInt
import com.danser.coloring.utils.drawme.FixedDrawMe
import com.rafakob.drawme.R
import com.rafakob.drawme.delegate.DrawMeShape
import com.rafakob.drawme.util.Coloring

open class FixedDrawMeShape @JvmOverloads constructor(
        context: Context,
        view: View,
        attrs: AttributeSet? = null,
        @AttrRes defStyleAttr: Int = 0
) : DrawMeShape(context, view, attrs, defStyleAttr), FixedDrawMe {

    private var maskPressedColor = Color.parseColor("#1F000000")


    override fun setBackgroundColor(@ColorInt color: Int) {
        backColor = color
        updateBackColors()
        updateLayout()
    }

    override fun getBackgroundColor(): Int = backColor

    override fun setStrokeColor(@ColorInt color: Int) {
        strokeColor = color
    }

    override fun setStrokeWidth(width: Int) {
        this.stroke = width
    }

    override fun setCornersRadius(tl: Int, tr: Int, bl: Int, br: Int){
        radiusTopLeft = tl
        radiusTopRight = tr
        radiusBottomLeft = bl
        radiusBottomRight = br
    }

    override fun setRippleEffectEnabled(enabled: Boolean) {
        rippleEffect = enabled
        rippleUseControlHighlight = enabled
        maskColorPressed = if (enabled) this.maskPressedColor else Color.TRANSPARENT
        maskColorPressedInverse = if (enabled) this.maskColorPressedInverse else Color.TRANSPARENT
        updateBackColors()
        updateLayout()
    }

    override fun setRippleColor(@ColorInt color: Int) {
        maskColorPressedInverse = color
        maskPressedColor = color
        updateBackColors()
        updateLayout()
    }

    private fun updateBackColors() {
        backColorPressed = defaultPressedColor(backColor)
        backColorDisabled = defaultDisabledColor(backColor)
    }

    @ColorInt
    private fun defaultPressedColor(normalColor: Int): Int = when {
        maskBrightnessThreshold > 0.0f && Coloring.getColorBrightness(normalColor) < maskBrightnessThreshold -> {
            Coloring.mix(maskColorPressedInverse, normalColor)
        }
        Build.VERSION.SDK_INT >= 21 && rippleUseControlHighlight -> Coloring.getThemeColor(
            mView.context,
            R.attr.colorControlHighlight
        )
        else -> Coloring.mix(maskColorPressed, normalColor)
    }

    @ColorInt
    private fun defaultDisabledColor(normalColor: Int): Int = Coloring.mix(maskColorDisabled, normalColor)

}
