package com.danser.coloring.utils

import android.os.Handler
import android.view.View
import java.util.concurrent.atomic.AtomicBoolean

/**
 * This class debounces a given action on DELAY_MS time
 * It helps not to open activities and fragments twice on fast clicks, etc.
 *
 * @author danser on 30.05.17.
 */
fun View.setDebounceOnClickListener(listener: (View) -> Unit) {
    setOnClickListener(DebounceClickListener(doClick = listener))
}

class DebounceClickListener(
    delay: Long = DEFAULT_DEBOUNCE_DELAY,
    private val doClick: (View) -> Unit
) : View.OnClickListener {

    private val debouncer: Debouncer =
        Debouncer(delay)

    override fun onClick(v: View) {
        debouncer.debounceAction { doClick(v) }
    }
}

class Debouncer(private val delayMs: Long = DEFAULT_DEBOUNCE_DELAY) {

    private val handler = Handler()

    private val disabled = AtomicBoolean(false)

    fun debounceAction(action: () -> Unit) {
        if (disabled.getAndSet(true)) {
            return
        }
        action()
        debounce()
    }

    private fun debounce() {
        handler.postDelayed({ disabled.set(false) }, delayMs)
    }

}

const val DEFAULT_DEBOUNCE_DELAY = 1000L
