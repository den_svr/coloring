package com.danser.coloring.utils.drawme

import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.util.AttributeSet
import android.view.View
import androidx.annotation.ColorInt
import com.danser.coloring.R

/**
 * This is common implementation of FixedDrawMe-views.
 * By default DrawMe-views don`t allow to change attributes programmatically, so this class fix it.
 * To extends another views see for example [FixedDrawMeLinearLayout]
 * @author airfreshener on 21.03.2018.
 */
open class FixedDrawMeViewDelegate : IFixedDrawMeViewDelegate, IAspectRatioView by BaseAspectRatioView() {

    lateinit var drawMe: FixedDrawMe
    protected var view: View? = null
    private var aspectRatioEnabled: Boolean = false
    private var backgroundFromLayout: Drawable? = null

    override fun initDrawMe(
        view: View,
        attrs: AttributeSet?,
        defStyleAttr: Int
    ) {
        var a = view.context.obtainStyledAttributes(attrs, R.styleable.DrawMe, defStyleAttr, 0)
        aspectRatioEnabled = a.getBoolean(R.styleable.DrawMe_aspectRatioEnabled, aspectRatioEnabled)
        a.recycle()
        a = view.context.obtainStyledAttributes(attrs, intArrayOf(android.R.attr.background))
        backgroundFromLayout = a.getDrawable(0)
        a.recycle()
        createDrawMeShape(view, attrs, defStyleAttr)
        this.view = view
        initAspectRatio(view, attrs)
    }

    open fun createDrawMeShape(view: View, attrs: AttributeSet?, defStyleAttr: Int = 0) {
        drawMe = FixedDrawMeShape(view.context, view, attrs, defStyleAttr)
    }

    override fun onMeasureView(widthMeasureSpec: Int, heightMeasureSpec: Int): IntArray {
        var widthSpec = widthMeasureSpec
        var heightSpec = heightMeasureSpec
        if (aspectRatioEnabled) {
            val aspectSizes = measureView(widthSpec, heightSpec)
            widthSpec = aspectSizes[0]
            heightSpec = aspectSizes[1]
        }
        return drawMe.onMeasure(widthSpec, heightSpec)
    }


    @Suppress("DEPRECATION")
    override fun onLayoutView(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        val view = view ?: return
        val leftPadding = view.paddingLeft
        val rightPadding = view.paddingRight
        val topPadding = view.paddingTop
        val bottomPadding = view.paddingBottom

        drawMe.onLayout(changed, left, top, right, bottom)
        val backgroundDrawable = view.background
        val resourceDrawable = backgroundFromLayout
        val drawableList = ArrayList<Drawable>(2)
        if (resourceDrawable != null) drawableList.add(resourceDrawable)
        if (backgroundDrawable != null) drawableList.add(backgroundDrawable)
        if (drawableList.size > 1) {
            val actualDrawable = LayerDrawable(drawableList.toArray(arrayOf()))
            view.background = actualDrawable
        }

        view.setPadding(leftPadding, topPadding, rightPadding, bottomPadding)//to ignoring drawable padding
    }

    override fun setCornersRadius(tl: Int, tr: Int, bl: Int, br: Int) {
        drawMe.setCornersRadius(tl, tr, bl, br)
        view?.requestLayout()
    }

    override fun setCornersRadius(radius: Int) = setCornersRadius(radius, radius, radius, radius)

    override fun setBackColor(@ColorInt color: Int) {
        drawMe.setBackgroundColor(color)
        view?.requestLayout()
    }

    override fun setRippleEffectEnabled(enabled: Boolean) {
        drawMe.setRippleEffectEnabled(enabled)
        view?.requestLayout()
    }

    override fun setRippleColor(@ColorInt color: Int) {
        drawMe.setRippleColor(color)
        view?.requestLayout()
    }

    override fun getBackColor(): Int = drawMe.getBackgroundColor()

    override fun setStrokeColor(@ColorInt color: Int) {
        drawMe.setStrokeColor(color)
        view?.requestLayout()
    }

    override fun setStrokeWidth(width: Int) {
        drawMe.setStrokeWidth(width)
        view?.requestLayout()
    }

}
