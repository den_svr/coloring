package com.danser.coloring.utils.drawme

import android.util.AttributeSet
import android.view.View
import androidx.annotation.ColorInt
import com.danser.coloring.utils.drawme.IAspectRatioView

interface IFixedDrawMeViewDelegate : IAspectRatioView {
    fun onMeasureView(widthMeasureSpec: Int, heightMeasureSpec: Int): IntArray
    fun onLayoutView(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int)
    fun setCornersRadius(tl: Int, tr: Int, bl: Int, br: Int)
    fun setCornersRadius(radius: Int)
    fun setBackColor(@ColorInt color: Int)
    fun getBackColor(): Int
    fun setStrokeColor(@ColorInt color: Int)
    fun setStrokeWidth(width: Int)
    fun setRippleEffectEnabled(enabled: Boolean)
    fun setRippleColor(@ColorInt color: Int)
    fun initDrawMe(view: View, attrs: AttributeSet?, defStyleAttr: Int = 0)
}
