package com.danser.coloring.utils.drawme

import android.util.AttributeSet
import android.view.View

interface IAspectRatioView {

    fun initAspectRatio(view: View, attrs: AttributeSet?)

    /**
     * @param ratio - new ratio to set, should be greater or equal 0.0
     */
    fun setRatio(ratio: Float)

    fun setRatioMode(ratioMode: RatioMode)

    fun measureView(widthSpec: Int, heightSpec: Int): IntArray

    enum class RatioMode(val id: Int) {

        BY_WIDTH(1), BY_HEIGHT(2);

        companion object {
            fun valueOf(id: Int): RatioMode = when(id){
                BY_WIDTH.id -> BY_WIDTH
                BY_HEIGHT.id -> BY_HEIGHT
                else -> error("Unknown ratio mode")
            }
        }
    }

    fun Int.toExactly() = View.MeasureSpec.makeMeasureSpec(this, View.MeasureSpec.EXACTLY)

}
