package com.danser.coloring.utils

import android.graphics.Paint
import android.view.View.LAYER_TYPE_HARDWARE
import android.view.ViewGroup
import androidx.annotation.ColorRes
import com.danser.coloring.R
import com.example.delegateadapter.delegate.KDelegateAdapter
import com.example.delegateadapter.delegate.diff.IComparableItem
import kotlinx.android.synthetic.main.item_complex_divider.*
import java.io.Serializable

object DividerAdapter : KDelegateAdapter<DividerItem>() {

    override fun getLayoutId(): Int = R.layout.item_complex_divider

    override fun onBind(item: DividerItem, viewHolder: KViewHolder) = with(viewHolder) {
        containerView.setBackgroundResource(item.backgroundColor)
        with(vDivider) {
            setBackgroundResource(item.dividerColor)
            setLayerType(LAYER_TYPE_HARDWARE, Paint())
            vDivider.layoutParams = (vDivider.layoutParams as ViewGroup.MarginLayoutParams).apply {
                height = item.height.toPixels(context)
                setMargins(
                    item.marginLeft?.toPixels(context) ?: 0,
                    item.marginTop?.toPixels(context) ?: 0,
                    item.marginRight?.toPixels(context) ?: 0,
                    item.marginBottom?.toPixels(context) ?: 0
                )
            }
        }
    }

    override fun isForViewType(items: MutableList<*>, position: Int): Boolean =
        items[position] is DividerItem


}

data class DividerItem(
    @ColorRes val dividerColor: Int = android.R.color.transparent,
    val backgroundColor: Int = android.R.color.transparent,
    val height: Resources.Dimen = 16.toDimenRes(),
    val marginLeft: Resources.Dimen? = null,
    val marginRight: Resources.Dimen? = null,
    val marginTop: Resources.Dimen? = null,
    val marginBottom: Resources.Dimen? = null,
    private val id: String = dividerColor.toString(16)
) : IComparableItem, Serializable {

    override fun id(): Any = id

    override fun content(): Any = this
}
