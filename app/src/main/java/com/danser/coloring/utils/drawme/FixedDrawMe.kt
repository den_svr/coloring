package com.danser.coloring.utils.drawme

import androidx.annotation.ColorInt
import com.rafakob.drawme.delegate.DrawMe

interface FixedDrawMe : DrawMe {

    fun setBackgroundColor(@ColorInt color: Int)

    fun getBackgroundColor(): Int

    fun setStrokeColor(@ColorInt color: Int)

    fun setStrokeWidth(width: Int)

    fun setCornersRadius(tl: Int, tr: Int, bl: Int, br: Int)

    fun setRippleEffectEnabled(enabled: Boolean)

    fun setRippleColor(@ColorInt color: Int)
}
