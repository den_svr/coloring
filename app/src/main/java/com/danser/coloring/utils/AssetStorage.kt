package com.danser.coloring.utils

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.IOException
import java.io.InputStream


class AssetStorage(private val context: Context) {

    inline fun <reified T> readJsonAsset(assetPath: String): T {
        val jsonString = getJsonString(assetPath)
        return Gson().fromJson(jsonString, object : TypeToken<T>() {}.type)
    }

    fun getDrawableId(fileName: String): Int = context.resources
        .getIdentifier(fileName, "drawable", context.packageName)

    fun getBitmapFromAsset(filePath: String): Bitmap? {
        val assetManager = context.assets
        val istr: InputStream
        var bitmap: Bitmap? = null
        try {
            istr = assetManager.open(filePath)
            bitmap = BitmapFactory.decodeStream(istr)
        } catch (e: IOException) {
            // handle exception
        }
        return bitmap
    }

    fun getJsonString(assetPath: String): String {
        var jsonString = ""
        var _stream: InputStream? = null
        try {
            val stream: InputStream = context.assets.open(assetPath)
            _stream = stream
            val size = stream.available()
            val buffer = ByteArray(size)
            stream.read(buffer)
            jsonString = String(buffer)
        } catch (e: Throwable) {
            Log.e(TAG, "parsing error, path = $assetPath", e)
        } finally {
            _stream?.let { closeSilently(it) }
        }
        return jsonString
    }

    private fun closeSilently(stream: InputStream?): Boolean {
        if (stream != null) {
            try {
                stream.close()
                return true
            } catch (e: IOException) {
                Log.e(TAG, "closing json reading stream failed", e)
                return false
            }
        } else {
            return false
        }
    }

    companion object {
        private val TAG = AssetStorage::class.java.simpleName
    }
}
