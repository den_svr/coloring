package com.danser.coloring.utils

import android.app.Activity
import androidx.fragment.app.Fragment
import com.danser.coloring.MainApplication

val Activity.mainApplication get () = applicationContext as MainApplication

val Fragment.mainApplication get () = activity?.applicationContext as? MainApplication
