package com.danser.coloring.utils

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color


fun Resources.loadBitmap(drawableRes: Int): Bitmap =
    BitmapFactory.decodeResource(this, drawableRes)

fun Bitmap.resize(width: Int, height: Int): Bitmap =
    Bitmap.createScaledBitmap(this, width, height, false)

fun Bitmap.resize(width: Int): Bitmap = resize(
    width = width,
    height = (width / this.width.toFloat() * this.height).toInt())

fun Int.toColorHex(): Int = 0xFF and this

fun Int.toColor(): Int = Integer.toHexString(this).let { hex ->
    val hex = "#" + hex.padStart(6, '0')
    Color.parseColor(hex)
}

fun String.hexToInt(): Int = replace("0x", "").toInt(16)
