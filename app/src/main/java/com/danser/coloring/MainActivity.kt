package com.danser.coloring

import android.animation.LayoutTransition
import android.os.Bundle
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.danser.coloring.domain.repository.AdController
import com.danser.coloring.domain.repository.getBannerAdId
import com.danser.coloring.presenter.GameEffect
import com.danser.coloring.view.fragment.GameFragment
import com.danser.coloring.view.fragment.MainFragment
import com.google.android.gms.ads.*
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private lateinit var adBanner: AdView

    private val adController: AdController by lazy {
        AdController(this) { effect ->
            val gameFragment = supportFragmentManager.findFragmentByTag(
                GameFragment::class.java.simpleName
            ) as GameFragment
            gameFragment.onEffect(effect)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            replaceFragment(MainFragment(), withBackStack = false)
        }

        initAdmob()
        loadBanner()

        adController.loadAd()

        vRoot.layoutTransition.enableTransitionType(LayoutTransition.CHANGING)
    }

    fun replaceFragment(fragment: Fragment, withBackStack: Boolean = true) {
        if (supportFragmentManager.findFragmentByTag(fragment::class.java.simpleName) != null) {
            return
        }
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.vMainContent, fragment, fragment::class.java.simpleName)
            .apply { if (withBackStack) addToBackStack(null) }
            .commit()
    }

    fun showInterstitialAd(pendingEffect: GameEffect) {
        adController.showAd(pendingEffect)
    }

    private fun initAdmob() {
        MobileAds.initialize(this)
        adBanner = AdView(this)
        adBanner.adSize = AdSize.BANNER
        adBanner.adUnitId = getBannerAdId()
        adContainer.addView(adBanner, FrameLayout.LayoutParams(MATCH_PARENT, MATCH_PARENT))
    }

    private fun loadBanner() {
        val adRequest = AdRequest.Builder().build()
        adBanner.loadAd(adRequest)
        adBanner.adListener = object : AdListener() {
            override fun onAdOpened() {
                adBanner.destroy()
                adBanner.isVisible = false
            }
        }
    }
}
