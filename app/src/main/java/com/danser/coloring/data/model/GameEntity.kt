package com.danser.coloring.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverter
import com.danser.coloring.domain.model.Segment
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


@Entity
class GameEntity(
    @PrimaryKey
    var id: String = "",
    var segments: List<Segment> = emptyList(),
    var locked: Boolean = true,
    var finished: Boolean = false
) {
    constructor() : this(id = "")
}

class SegmentConverter {

    private val gson = Gson()

    @TypeConverter
    fun from(segments: List<Segment>?): String {
        return gson.toJson(segments)
    }

    @TypeConverter
    fun to(json: String): List<Segment>? {
        return gson.fromJson(json, object : TypeToken<List<Segment>>() {}.type)
    }
}
