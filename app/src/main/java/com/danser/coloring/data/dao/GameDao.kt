package com.danser.coloring.data.dao

import androidx.room.*
import com.danser.coloring.data.model.GameEntity


@Dao
interface GameDao {

    @Query("SELECT * FROM GameEntity")
    fun getAll(): List<GameEntity>

    @Query("SELECT id FROM GameEntity WHERE finished = 1")
    fun getFinishedIds(): List<String>

    @Query("SELECT * FROM GameEntity WHERE id = :id")
    fun getById(id: String): GameEntity?

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(entity: GameEntity)

    @Delete
    fun delete(entity: GameEntity)
}
