package com.danser.coloring.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.danser.coloring.data.dao.GameDao
import com.danser.coloring.data.model.GameEntity
import com.danser.coloring.data.model.SegmentConverter

@Database(entities = [GameEntity::class], version = 1)
@TypeConverters(SegmentConverter::class)
abstract class MainDatabase: RoomDatabase() {
    abstract fun gameDao(): GameDao
}
