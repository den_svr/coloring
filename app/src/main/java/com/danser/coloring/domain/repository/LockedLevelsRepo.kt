package com.danser.coloring.domain.repository

import android.content.Context
import android.content.SharedPreferences


interface LockedLevelsRepo {
    fun isLocked(levelId: String): Boolean
    fun unlock(levelId: String)
    fun getUnlockedLevelIds(): Set<String>
}

class LockedLevelsRepoImpl(private val appContext: Context) : LockedLevelsRepo {

    private val prefs: SharedPreferences by lazy {
        appContext.getSharedPreferences("locked_level_prefs", Context.MODE_PRIVATE)
    }

    override fun isLocked(levelId: String): Boolean = !getUnlockedLevelIds().contains(levelId)

    override fun unlock(levelId: String) {
        val ids = getUnlockedLevelIds() + levelId
        prefs.edit().putStringSet(UNLOCKED_LEVELS_KEY, ids).apply()
    }

    override fun getUnlockedLevelIds(): Set<String> =
        (prefs.getStringSet(UNLOCKED_LEVELS_KEY, emptySet()) ?: emptySet())

    companion object {
        private const val UNLOCKED_LEVELS_KEY = "UNLOCKED_LEVELS"
    }
}
