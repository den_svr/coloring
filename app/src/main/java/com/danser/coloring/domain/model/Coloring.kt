package com.danser.coloring.domain.model

import android.graphics.Bitmap

data class Coloring(
    val id: String,
    val segments: List<Segment>,
    val image: Bitmap,
    val mask: Bitmap,
    val finishBitmapName: String
)

fun Coloring.isColored(): Boolean = segments.all { it.colored }

fun Coloring.colorSegment(maskColorHex: Int): Coloring {
    val segments = segments.map { segment ->
        when (segment.maskColorHex) {
            maskColorHex -> segment.copy(colored = true)
            else -> segment
        }
    }
    return copy(
        segments = segments
    )
}

fun Coloring.getNextSelectedColor(selectedColor: Int): Int {
    val allSegmentsOfSelectedColorColored = segments
        .filter { it.colorHex == selectedColor }
        .all { it.colored }
    return when {
        allSegmentsOfSelectedColorColored ->
            segments
                .groupBy { it.colorHex }
                .toList()
                .firstOrNull { (_, segments) -> segments.any { !it.colored } }
                ?.first ?: selectedColor
        else -> selectedColor
    }
}

fun Coloring.getColoredSegments(colored: Boolean = true): List<Segment> =
    segments.filter { it.colored == colored }
