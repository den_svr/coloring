package com.danser.coloring.domain.repository

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import java.io.File
import java.io.FileOutputStream
import java.io.IOException


interface BitmapRepo {
    fun saveBitmap(bitmap: Bitmap, path: String)
    fun loadBitmap(path: String): Bitmap?
    fun removeBitmap(path: String)
}

class BitmapRepoImpl(appContext: Context) : BitmapRepo {

    private val dirPath: String

    init {
        val dir = File(appContext.filesDir, "bitmaps")
        dirPath = dir.canonicalPath
        if (!dir.exists()) {
            dir.mkdir()
        }
    }

    override fun saveBitmap(bitmap: Bitmap, path: String) {
        try {
            FileOutputStream(path.fullPath).use { out ->
                bitmap.compress(Bitmap.CompressFormat.PNG, 95, out)
            }
        } catch (e: IOException) {
            Log.e("BitmapExt", "Can't save bitmap to path `${path.fullPath}`", e)
        }
    }

    override fun loadBitmap(path: String): Bitmap? = try {
        when {
            File(path.fullPath).exists() -> BitmapFactory.decodeFile(path.fullPath)
            else -> null
        }
    } catch (e: Exception) {
        Log.e("BitmapExt", "Can't load bitmap from path `${path.fullPath}`", e)
        null
    }

    override fun removeBitmap(path: String) {
        try {
            File(dirPath + path).apply {
                if (exists()) {
                    delete()
                }
            }
        } catch (e: Exception) {
            Log.e("BitmapExt", "Can't remove file with path `$path`", e)
        }
    }

    private val String.fullPath: String
        get() = dirPath + "/" + this
}
