package com.danser.coloring.domain.model

import android.graphics.Bitmap

data class Levels(
    val levels: List<Level> = emptyList()
)

data class Level(
    val id: String,
    val preview: Bitmap,
    val locked: Boolean
)

fun Levels.nextLevel(levelId: String): Level? {
    val index = levels.indexOfFirst { it.id == levelId }
    if (index == -1) return null
    return levels.getOrNull(index + 1)
}

fun Levels.unlockLevel(levelId: String): Levels = this.copy(
    levels = levels.map { level ->
        if (level.id == levelId) {
            level.copy(
                locked = false
            )
        } else {
            level
        }
    }
)

fun Levels.hasNext(levelId: String): Boolean {
    val index = levels.indexOfFirst { it.id == levelId }
    if (index == -1) return false
    return index < levels.size
}

fun Levels.level(levelId: String): Level? = levels.firstOrNull { it.id == levelId }
