package com.danser.coloring.domain.repository

import com.danser.coloring.data.database.MainDatabase
import com.danser.coloring.data.model.GameEntity
import com.danser.coloring.domain.model.Coloring
import com.danser.coloring.domain.model.isColored

interface ISavedGameRepository {
    fun getGame(id: String): GameEntity?
    fun saveGame(coloring: Coloring)
    fun resetGame(coloring: Coloring)
}

class SavedGameRepository(
    private val database: MainDatabase
) : ISavedGameRepository {

    override fun getGame(id: String): GameEntity? = database.gameDao().getById(id)

    override fun saveGame(coloring: Coloring) {
        val gameEntity = GameEntity(
            id = coloring.id,
            segments = coloring.segments,
            finished = coloring.isColored()
        )
        database.gameDao().insert(gameEntity)
    }

    override fun resetGame(coloring: Coloring) {
        val segments = coloring.segments.map { segment ->
            segment.copy(
                colored = false
            )
        }
        val gameEntity = GameEntity(
            id = coloring.id,
            segments = segments,
            finished = false
        )
        database.gameDao().insert(gameEntity)
    }
}
