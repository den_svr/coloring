package com.danser.coloring.domain.repository

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.ConflatedBroadcastChannel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow

interface IMusicRepository {
    fun switchMusic()

    val observeMusicEnabled: StateFlow<Boolean>
}

class MusicRepository(context: Context) : IMusicRepository {

    @OptIn(ExperimentalCoroutinesApi::class)
    private val stateFlow: MutableStateFlow<Boolean> = MutableStateFlow(true)

    private val prefs: SharedPreferences =
        context.getSharedPreferences("coloring_prefs", MODE_PRIVATE)

    override val observeMusicEnabled: StateFlow<Boolean> = stateFlow

    init {
        stateFlow.value = prefs.getBoolean(MUSIC_ENABLED_KEY, true)
    }

    override fun switchMusic() {
        if (stateFlow.value) {
            stopMusic()
        } else {
            startMusic()
        }
    }

    private fun startMusic() {
        stateFlow.value = true
        prefs.edit().putBoolean(MUSIC_ENABLED_KEY, true).apply()
    }

    private fun stopMusic() {
        stateFlow.value = false
        prefs.edit().putBoolean(MUSIC_ENABLED_KEY, false).apply()
    }

    companion object {
        private const val MUSIC_ENABLED_KEY = "MUSIC_ENABLED"
    }
}
