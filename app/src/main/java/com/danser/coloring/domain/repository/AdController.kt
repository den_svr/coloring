package com.danser.coloring.domain.repository

import android.app.Activity
import android.util.Log
import com.danser.coloring.BuildConfig
import com.danser.coloring.MainApplication
import com.danser.coloring.presenter.GameEffect
import com.google.android.gms.ads.AdListener
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd


class AdController(
    private val activity: Activity,
    private val onAdClosed: (GameEffect) -> Unit
) {

    private var interstitialAd: InterstitialAd? = null
    private val application: MainApplication = activity.application as MainApplication

    fun loadAd() {
        val adRequest = AdRequest.Builder().build()
        interstitialAd = InterstitialAd(activity).apply {
            adUnitId = getInterstitialAdId()
            try {
                loadAd(adRequest)
            } catch (e: Throwable) {
                Log.e(AdController::class.java.simpleName, "failed to load ad", e)
            }
        }
    }

    fun showAd(pendingEffect: GameEffect) {
        val ad = interstitialAd ?: return
        if (ad.isLoaded) {
            ad.adListener = object : AdListener() {
                override fun onAdOpened() {
                    super.onAdOpened()
                    activity.application
                    application.pauseMusic()
                }

                override fun onAdClosed() {
                    super.onAdClosed()
                    interstitialAd = null
                    application.startMusic()
                    loadAd()
                    onAdClosed(pendingEffect)
                }
            }
            try {
                ad.show()
            } catch (e: Throwable) {
                Log.e(AdController::class.java.simpleName, "failed to show ad", e)
            }
        }
    }
}

fun getBannerAdId(): String = when (BuildConfig.DEBUG) {
    true -> "ca-app-pub-3940256099942544/6300978111"
    false -> "ca-app-pub-4939936399706276/2733830850"
}

private fun getInterstitialAdId(): String = when (BuildConfig.DEBUG) {
    true -> "ca-app-pub-3940256099942544/1033173712"
    false -> "ca-app-pub-4939936399706276/7573060736"
}