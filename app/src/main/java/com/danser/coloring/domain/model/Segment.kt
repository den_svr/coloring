package com.danser.coloring.domain.model

data class Segment(
    val maskColorHex: Int,
    val colorHex: Int,
    val colorLabel: String,
    val colored: Boolean = false
)
