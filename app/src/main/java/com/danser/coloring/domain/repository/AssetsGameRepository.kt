package com.danser.coloring.domain.repository

import com.danser.coloring.domain.model.Coloring
import com.danser.coloring.domain.model.Segment
import com.danser.coloring.utils.AssetStorage
import com.danser.coloring.utils.hexToInt
import com.danser.coloring.utils.resize

interface IGameRepository {
    fun getGame(levelId: String): Coloring
}

class AssetsGameRepository(private val assetStorage: AssetStorage) : IGameRepository {

    override fun getGame(levelId: String): Coloring {
        val coloring = assetStorage.readJsonAsset<NWColoring>("levels/$levelId/level.json")
        val image = assetStorage.getBitmapFromAsset("levels/${coloring.coloringImage}")!!
        val mask = assetStorage.getBitmapFromAsset("levels/${coloring.maskImage}")!!
            .resize(image.width, image.height)
        val segments = coloring.segments.flatMap { segment ->
            val maskFrom = segment.maskFrom.hexToInt()
            val maskTo = segment.maskTo.hexToInt()
            val color = segment.color.hexToInt()
            (maskFrom..maskTo)
                .map { maskColorHex ->
                    Segment(
                        maskColorHex = maskColorHex,
                        colorHex = color,
                        colorLabel = segment.colorLabel
                    )
                }
        }
        return Coloring(
            id = coloring.id,
            image = image,
            mask = mask,
            finishBitmapName = coloring.resultImage,
            segments = segments
        )
    }
}

class NWColoring(
    val id: String,
    val coloringImage: String,
    val maskImage: String,
    val resultImage: String,
    val segments: List<NWSegment>
)

class NWSegment(
    val maskFrom: String,
    val maskTo: String,
    val color: String,
    val colorLabel: String
)
