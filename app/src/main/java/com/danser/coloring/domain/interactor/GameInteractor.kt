package com.danser.coloring.domain.interactor

import com.danser.coloring.domain.model.Coloring
import com.danser.coloring.domain.repository.IGameRepository
import com.danser.coloring.domain.repository.LevelPreviewsRepository
import com.danser.coloring.domain.repository.SavedGameRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class GameInteractor(
    private val gameRepository: IGameRepository,
    private val savedGameRepository: SavedGameRepository,
    private val ioScope: CoroutineScope
) {

    suspend fun getGame(levelId: String): Coloring = withContext(ioScope.coroutineContext) {
        val game = gameRepository.getGame(levelId)
        val savedGame = savedGameRepository.getGame(levelId) ?: return@withContext game
        game.copy(
            segments = savedGame.segments
        )
    }

    suspend fun saveGame(game: Coloring) = withContext(ioScope.coroutineContext) {
        savedGameRepository.saveGame(game)
    }

    suspend fun resetGame(game: Coloring) = withContext(ioScope.coroutineContext)  {
        savedGameRepository.resetGame(game)
    }
}
