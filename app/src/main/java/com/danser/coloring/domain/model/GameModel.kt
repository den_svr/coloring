package com.danser.coloring.domain.model

data class GameModel(
    val coloring: Coloring,
    val selectedColor: Int,
    val isFinished: Boolean = false,
    val finishBitmapName: String
)
