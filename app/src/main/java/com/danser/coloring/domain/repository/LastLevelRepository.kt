package com.danser.coloring.domain.repository

import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences

interface ILastLevelRepository {
    fun getLastLevelId(): String?
    fun saveLastLevelId(id: String)
}

class LastLevelRepository(context: Context) : ILastLevelRepository {

    private val prefs: SharedPreferences =
        context.getSharedPreferences("coloring_prefs", MODE_PRIVATE)

    override fun getLastLevelId(): String? = prefs.getString(LAST_LEVEL_KEY, null)

    override fun saveLastLevelId(id: String) {
        prefs.edit().putString(LAST_LEVEL_KEY, id).apply()
    }

    companion object {
        private const val LAST_LEVEL_KEY = "LAST_LEVEL"
    }
}
