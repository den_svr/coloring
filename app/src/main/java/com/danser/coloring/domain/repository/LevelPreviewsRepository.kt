package com.danser.coloring.domain.repository

import android.graphics.Bitmap
import com.danser.coloring.domain.model.Level
import com.danser.coloring.domain.model.Levels
import com.danser.coloring.utils.AssetStorage
import com.danser.coloring.utils.resize
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.withContext

interface ILevelPreviewsRepository {
    suspend fun getLevels(): Levels
    suspend fun saveLevelPreview(levelPreview: Bitmap, levelId: String)
    suspend fun resetLevelPreview(levelId: String)
}

class LevelPreviewsRepository(
    private val assetStorage: AssetStorage,
    private val bitmapRepo: BitmapRepo,
    private val lockedLevelsRepo: LockedLevelsRepo,
    private val ioScope: CoroutineScope
) : ILevelPreviewsRepository {

    override suspend fun getLevels(): Levels = withContext(ioScope.coroutineContext) {
        val response =
            assetStorage.readJsonAsset<LevelPreviewsResponse>("levels/level_previews.json")
        val unlockedLevelIds = lockedLevelsRepo.getUnlockedLevelIds()
        val levels = response.levels.mapIndexed{ idx, level ->
            val image = bitmapRepo.loadBitmap("colored_preview_${level.id}.png")
                ?: assetStorage.getBitmapFromAsset("levels/${level.image}")!!
            Level(
                id = level.id,
                preview = image,
                locked = idx != 0 && !unlockedLevelIds.contains(level.id)
            )
        }
        Levels(levels = levels)
    }

    override suspend fun saveLevelPreview(levelPreview: Bitmap, levelId: String) =
        withContext(ioScope.coroutineContext) {
            levelPreview.resize(500)
            bitmapRepo.saveBitmap(levelPreview, "colored_preview_$levelId.png")
        }

    override suspend fun resetLevelPreview(levelId: String) =
        withContext(ioScope.coroutineContext) {
            bitmapRepo.removeBitmap("colored_preview_$levelId.png")
        }
}

class LevelPreviewsResponse(
    val levels: List<NWLevelPreview>
)

class NWLevelPreview(
    val id: String,
    val image: String
)
